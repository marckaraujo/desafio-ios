//
//  RepositoriesTableViewSpec.m
//  zazcar-desafio
//
//  Created by Marcus Ataide on 03/04/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <Expecta/Expecta.h>
#import <Specta/Specta.h>
#import <OCMock/OCMock.h>
#import "RepositoriesViewController.h"
#import "RepositoriesTableViewCell.h"

SpecBegin(RepositoriesTableViewSpec)

describe(@"RepositoriesTableView", ^{
    __block UITableViewController *vc;
    beforeAll(^{
        UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        RepositoriesViewController *controller = [main instantiateViewControllerWithIdentifier:@"RepositoriesTableViewController"];
        vc = (RepositoriesViewController *)controller;
        [vc view];
    });
    
    it(@"should exists", ^{
        expect(vc).toNot.beNil();
    });
    
    it(@"should respond to UITableViewDataSource required methods", ^{
        expect(vc).respondTo(@selector(tableView:numberOfRowsInSection:));
        expect(vc).respondTo(@selector(tableView:cellForRowAtIndexPath:));
    });
    
    it(@"should test Controller SetUp Cell Correctly", ^{
        id mockTableView = [OCMockObject mockForClass:[UITableView class]];
        RepositoriesTableViewCell *cell = [[RepositoriesTableViewCell alloc] init];
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [[[mockTableView expect] andReturn:cell] dequeueReusableCellWithIdentifier:@"RepositoriesCell" forIndexPath:indexPath];
        
        id result = [vc tableView:mockTableView cellForRowAtIndexPath:indexPath];
        
        expect(result).notTo.beNil();
        expect(result)._beIdenticalTo((__bridge void *)(cell));
        [mockTableView verify];
    });
    
});


SpecEnd
