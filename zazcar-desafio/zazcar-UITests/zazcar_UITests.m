//
//  zazcar_UITests.m
//  zazcar-UITests
//
//  Created by Marcus Ataide on 02/04/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <KIF/KIF.h>
#import <Expecta/Expecta.h>

@interface zazcar_UITests : XCTestCase

@end

static NSString * const KRepositoriesList = @"RepositoriesList";

@implementation zazcar_UITests


- (void)testLoadMoreRepositoriesUITableView {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    [tester waitForViewWithAccessibilityLabel:KRepositoriesList];
    // 1
    UITableView *tableView = (UITableView *)[tester waitForViewWithAccessibilityLabel:KRepositoriesList];
    [tester waitForTimeInterval: 4.0];
    NSInteger originalHistoryCount = [tableView numberOfRowsInSection:0];
    expect(originalHistoryCount).to.beGreaterThan(0);
    // 2
    [tester swipeViewWithAccessibilityLabel:@"Section 0 Row 0" inDirection:KIFSwipeDirectionUp];
    [tester swipeViewWithAccessibilityLabel:KRepositoriesList inDirection:KIFSwipeDirectionUp];
    [tester waitForTimeInterval: 4.0];
    // Compare
    NSInteger finalHistoryCount = [tableView numberOfRowsInSection:0];
    expect(originalHistoryCount).to.beLessThan(finalHistoryCount);
}


@end
