//
//  main.m
//  zazcar-desafio
//
//  Created by Marcus Ataide on 30/03/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
