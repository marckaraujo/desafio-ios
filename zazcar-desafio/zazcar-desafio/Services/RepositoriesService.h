//
//  RepositoriesService.h
//  zazcar-desafio
//
//  Created by Marcus Ataide on 01/04/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PromiseKit/PromiseKit.h>
#import "APIClient.h"
#import "RepositoriesModel.h"

@interface RepositoriesService : NSObject

@property (nonatomic, strong, readwrite) NSString *language;
@property(nonatomic, assign) int page;

+ (AnyPromise *)allRepositoriesfetchRepositoriesByLanguage:(NSString *)language forPage:(int)page;
- (instancetype)initWithLanguage: (NSString *)language forPage: (int )page;

@end
