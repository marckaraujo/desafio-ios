//
//  PullRequestService.m
//  zazcar-desafio
//
//  Created by Marcus Ataide on 01/04/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//

#import "PullRequestService.h"

@implementation PullRequestService

- (instancetype)initWithOwner: (NSString *)owner forRepository: (NSString *)repository
{
    self = [super init];
    if (self)
    {
        self.owner = owner;
        self.repository = repository;
    }
    return self;
}

#pragma mark - Fetching allPullRequests
+ (AnyPromise *)allPullRequestsWithOwner: (NSString *)owner forRepository: (NSString *)repository {
    return [[APIClient sharedClient] fetchPullRequestsWithOwner:owner forRepository:repository].then(^(NSArray *response) {
        NSValueTransformer *transformer = [MTLJSONAdapter arrayTransformerWithModelClass:[PullRequestModel class]];
        NSArray *pullRequests = [transformer transformedValue:response];
        return pullRequests;
    });
}

@end
