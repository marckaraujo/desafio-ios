//
//  RepositoriesService.m
//  zazcar-desafio
//
//  Created by Marcus Ataide on 01/04/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//

#import "RepositoriesService.h"

@implementation RepositoriesService

- (instancetype)initWithLanguage: (NSString *)language forPage: (int )page
{
    self = [super init];
    if (self)
    {
        self.language = language;
        self.page = page;
    }
    return self;
}

#pragma mark - Fetching allRepositories
+ (AnyPromise *)allRepositoriesfetchRepositoriesByLanguage:(NSString *)language forPage:(int)page {
    return [[APIClient sharedClient] fetchRepositoriesByLanguage:language forPage:page].then(^(NSDictionary *response) {
        NSValueTransformer *transformer = [MTLJSONAdapter arrayTransformerWithModelClass:[RepositoriesModel class]];
        NSArray *repositories = [transformer transformedValue:response[@"items"]];
        return repositories;
    });
}

@end
