//
//  PullRequestService.h
//  zazcar-desafio
//
//  Created by Marcus Ataide on 01/04/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PromiseKit/PromiseKit.h>
#import "APIClient.h"
#import "PullRequestModel.h"

@interface PullRequestService : NSObject

@property (nonatomic, strong, readwrite) NSString *owner;
@property (nonatomic, strong, readwrite) NSString *repository;

- (instancetype)initWithOwner: (NSString *)owner forRepository: (NSString *)repository;
+ (AnyPromise *)allPullRequestsWithOwner: (NSString *)owner forRepository: (NSString *)repository;

@end
