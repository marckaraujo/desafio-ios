//
//  WebViewController.m
//  zazcar-desafio
//
//  Created by Marcus Ataide on 01/04/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"WebView";
    
    //CustomLeftBaritem
    UIButton *presetButton = [[UIButton alloc] initWithFrame:CGRectMake(180, 0, 30, 30)];
    UIImage *presetBtnBG = [[UIImage imageNamed:@"left"] stretchableImageWithLeftCapWidth:12.0 topCapHeight:0.0];
    [presetButton setImage:presetBtnBG forState:UIControlStateNormal];
    UIBarButtonItem *presetButtonItem = [[UIBarButtonItem alloc] initWithCustomView:presetButton];
    [presetButton addTarget:self action:@selector(toggleLeftPanel:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = presetButtonItem;

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", self.stringURL]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (void)toggleLeftPanel:(__unused id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - WebView didFailLoadWithError
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    if ([error code] == -1003)
    {
        UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ERROR", @"Erro!")  message:NSLocalizedString(@"WRONG_END", @"O endereço está errado")  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        [alerta show];
    } else if ([error code] == -1009)
    {
        UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ERROR", @"Erro!")  message:NSLocalizedString(@"NO_CONNECTION", @"Sem conexão com a Internet")  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        [alerta show];
    }
}

@end
