//
//  WebViewController.h
//  zazcar-desafio
//
//  Created by Marcus Ataide on 01/04/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, copy) NSString *stringURL;

@end
