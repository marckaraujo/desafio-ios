//
//  RepositoriesModel.m
//  zazcar-desafio
//
//  Created by Marcus Ataide on 31/03/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//

#import "RepositoriesModel.h"

@implementation RepositoriesModel

#pragma mark - Mantle
+(NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"name": @"name",
             @"repoDescription": @"description",
             @"authorName": @"owner.login",
             @"thumbViewURL": @"owner.avatar_url",
             @"stars": @"stargazers_count",
             @"forks": @"forks_count"};
}

+ (NSValueTransformer *)thumbViewURLJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

@end
