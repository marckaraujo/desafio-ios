//
//  PullRequestModel.m
//  zazcar-desafio
//
//  Created by Marcus Ataide on 31/03/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//

#import "PullRequestModel.h"

@implementation PullRequestModel

#pragma mark - Mantle
+(NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"authorName": @"user.login",
             @"thumbViewURL": @"user.avatar_url",
             @"title": @"title",
             @"state": @"state",
             @"RequestDescription": @"body",
             @"date": @"created_at",
             @"htmlUrl": @"html_url"};
}

+ (NSValueTransformer *)thumbViewURLJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

+ (NSValueTransformer *)htmlUrlURLJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

+ (NSValueTransformer *)dateJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *str, BOOL *success, NSError **error){
        return [self.dateFormatter dateFromString:str];
    } reverseBlock:^(NSDate *date, BOOL *success, NSError **error) {
        return [self.dateFormatter stringFromDate:date];
    }];
}

#pragma mark - Formatter
+ (NSDateFormatter *)dateFormatter {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.Z";
    return dateFormatter;
}


@end
