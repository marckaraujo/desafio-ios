//
//  RepositoriesModel.h
//  zazcar-desafio
//
//  Created by Marcus Ataide on 31/03/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>


@interface RepositoriesModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, copy, readonly) NSString *repoDescription;
@property (nonatomic, copy, readonly) NSString *authorName;
@property (nonatomic, copy, readonly) NSString *thumbViewURL;
@property (nonatomic, copy, readonly) NSNumber *stars;
@property (nonatomic, copy, readonly) NSNumber *forks;

@end
