//
//  PullRequestTableViewCell.h
//  zazcar-desafio
//
//  Created by Marcus Ataide on 31/03/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullRequestModel.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

@interface PullRequestTableViewCell : UITableViewCell

@property (nonatomic, strong) PullRequestModel *pullRequest;
@property (weak, nonatomic) IBOutlet UIImageView *thumbImageView;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *pullRequestDescription;
@property (weak, nonatomic) IBOutlet UILabel *username;
@property (weak, nonatomic) IBOutlet UILabel *date;


@end
