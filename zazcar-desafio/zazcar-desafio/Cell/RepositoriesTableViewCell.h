//
//  RepositoriesTableViewCell.h
//  zazcar-desafio
//
//  Created by Marcus Ataide on 31/03/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RepositoriesModel.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface RepositoriesTableViewCell : UITableViewCell

@property (nonatomic, strong) RepositoriesModel *repositories;
@property (weak, nonatomic) IBOutlet UIImageView *thumbImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleName;
@property (weak, nonatomic) IBOutlet UILabel *repoDescription;
@property (weak, nonatomic) IBOutlet UILabel *forkCount;
@property (weak, nonatomic) IBOutlet UILabel *starCount;
@property (weak, nonatomic) IBOutlet UILabel *username;

@end
