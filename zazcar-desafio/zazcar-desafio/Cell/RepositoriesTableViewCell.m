//
//  RepositoriesTableViewCell.m
//  zazcar-desafio
//
//  Created by Marcus Ataide on 31/03/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//

#import "RepositoriesTableViewCell.h"

@implementation RepositoriesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _thumbImageView.clipsToBounds = YES;
    [_thumbImageView setContentMode:UIViewContentModeScaleAspectFit];
    CALayer *imageLayer = _thumbImageView.layer;
    [imageLayer setCornerRadius:imageLayer.frame.size.width/2];
    
    [imageLayer setBorderWidth:3];
    [imageLayer setBorderColor:[UIColor whiteColor].CGColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
}

- (void)setRepositories:(RepositoriesModel *)repositories {
    
    _titleName.text = repositories.name;
    _username.text = repositories.authorName;
    _repoDescription.text = repositories.repoDescription;
    _forkCount.text = [NSString stringWithFormat:@"%@", repositories.forks];
    _starCount.text = [NSString stringWithFormat:@"%@", repositories.stars];
    
    [_thumbImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", repositories.thumbViewURL]]
                      placeholderImage:[UIImage imageNamed:@"male.png"] 
                                options:SDWebImageRefreshCached];
    

    [self setNeedsLayout];
}

@end
