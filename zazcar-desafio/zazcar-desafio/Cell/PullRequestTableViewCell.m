//
//  PullRequestTableViewCell.m
//  zazcar-desafio
//
//  Created by Marcus Ataide on 31/03/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//

#import "PullRequestTableViewCell.h"

@implementation PullRequestTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _thumbImageView.clipsToBounds = YES;
    [_thumbImageView setContentMode:UIViewContentModeScaleAspectFit];
    CALayer *imageLayer = _thumbImageView.layer;
    [imageLayer setCornerRadius:imageLayer.frame.size.width/2];
    
    [imageLayer setBorderWidth:3];
    [imageLayer setBorderColor:[UIColor whiteColor].CGColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setPullRequest:(PullRequestModel *)pullRequest {
    
    _title.text = pullRequest.title;
    _username.text = pullRequest.authorName;
    _pullRequestDescription.text = pullRequest.RequestDescription;
    _date.text = [self dateFormatter:pullRequest.date];
    
    //CacheImageUsingAFNetworking
    __weak UIImageView *weakImageView = _thumbImageView;
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", pullRequest.thumbViewURL]] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:60];
    
    [_thumbImageView setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"male.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        UIImageView *strongImage = weakImageView;
        [strongImage setImage:image];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        
    }];
    
    [self setNeedsLayout];
}

- (NSString *)dateFormatter:(NSDate *)date {
    static NSDateFormatter *dateFormatter = nil;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateStyle = NSDateIntervalFormatterMediumStyle;
        dateFormatter.timeStyle = NSDateIntervalFormatterNoStyle;
    }
    return [dateFormatter stringFromDate:date];
}

@end
