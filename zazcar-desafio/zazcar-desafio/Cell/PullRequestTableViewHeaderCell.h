//
//  PullRequestTableViewHeaderCell.h
//  zazcar-desafio
//
//  Created by Marcus Ataide on 03/04/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PullRequestTableViewHeaderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *opened;
@property (weak, nonatomic) IBOutlet UILabel *closed;

@end
