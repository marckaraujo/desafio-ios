//
//  AppDelegate.h
//  zazcar-desafio
//
//  Created by Marcus Ataide on 30/03/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

