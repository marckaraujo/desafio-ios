//
//  APIClient.m
//  zazcar-desafio
//
//  Created by Marcus Ataide on 31/03/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//

#import "APIClient.h"

@implementation APIClient

static NSString * const APIClientAPIBaseURLString = @"https://api.github.com/";

+ (instancetype)sharedClient {
    static APIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[APIClient alloc] initWithBaseURL:[NSURL URLWithString:APIClientAPIBaseURLString]];
        _sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    });
    
    return _sharedClient;
}


#pragma mark - fetchRepositoriesByLanguage
- (AnyPromise *)fetchRepositoriesByLanguage:(NSString *)language forPage:(int)page {
    NSString *urlString = [NSString stringWithFormat:@"search/repositories?q=language:%@&sort=stars&page=%d", language, page];
    
    return [self fetchWithURLString:urlString].then(^(NSDictionary *response){
        return response;
    });
}

#pragma mark - fetchPullRequestsWithOwner
- (AnyPromise *)fetchPullRequestsWithOwner:(NSString *)owner forRepository:(NSString *)repository {
    NSString *urlString = [NSString stringWithFormat:@"repos/%@/%@/pulls?state=all", owner, repository];
    
    return [self fetchWithURLString:urlString].then(^(NSArray *response) {
        return response;
    });
}

- (AnyPromise *)fetchWithURLString:(NSString *)stringURL {
    return [AnyPromise promiseWithAdapterBlock:^(PMKAdapter  _Nonnull adapter) {
        
        NSURL *URL = [NSURL URLWithString:stringURL];
        [[APIClient sharedClient] GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            //NSLog(@"JSON: %@", responseObject);
            NSError *error;
            adapter(responseObject,error);
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
        
    }];
}


@end
