//
//  APIClient.h
//  zazcar-desafio
//
//  Created by Marcus Ataide on 31/03/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import <PromiseKit/PromiseKit.h>
#import "RepositoriesModel.h"
#import "PullRequestModel.h"
#import <AFNetworking/AFNetworking.h>


@interface APIClient : AFHTTPSessionManager

+ (instancetype)sharedClient;
- (instancetype)init NS_UNAVAILABLE;

- (AnyPromise *)fetchRepositoriesByLanguage:(NSString *)language forPage:(int)page;

- (AnyPromise *)fetchPullRequestsWithOwner:(NSString *)owner forRepository:(NSString *)repository;

@end
