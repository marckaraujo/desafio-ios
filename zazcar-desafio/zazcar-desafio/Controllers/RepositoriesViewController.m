//
//  RepositoriesViewController.m
//  zazcar-desafio
//
//  Created by Marcus Ataide on 30/03/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//

#import "RepositoriesViewController.h"

static NSString * const reuseIdentifier = @"RepositoriesCell";
static NSString * const defaultLanguage = @"Java";
static int initialPage = 1;

@interface RepositoriesViewController ()

@end

@implementation RepositoriesViewController

#pragma mark - viewDidLoad


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationItem.title = @"Github JavaPop";
    
    //CustomLeftBaritem
    UIButton *presetButton = [[UIButton alloc] initWithFrame:CGRectMake(180, 0, 30, 30)];
    UIImage *presetBtnBG = [[UIImage imageNamed:@"menu"] stretchableImageWithLeftCapWidth:12.0 topCapHeight:0.0];
    [presetButton setImage:presetBtnBG forState:UIControlStateNormal];
    UIBarButtonItem *presetButtonItem = [[UIBarButtonItem alloc] initWithCustomView:presetButton];
    self.navigationItem.leftBarButtonItem = presetButtonItem;
    
    //SetupHeaderView
    self.refreshControl = [[UIRefreshControl alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.frame.size.width, 100.0f)];
    [self.refreshControl addTarget:self action:@selector(fetchRepositories:) forControlEvents:UIControlEventValueChanged];
    [self.tableView.tableHeaderView addSubview:self.refreshControl];
    
    //SetupFooterView
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [activity setFrame:CGRectMake((self.view.frame.size.width / 2) -10 , 15, 20 , 20)];
    
    [footerView addSubview:activity];
    [self.tableView setTableFooterView:footerView];
    [self.tableView.tableFooterView setHidden:YES];
    [UIView setAnimationsEnabled:NO];
    
    [self.tableView setAccessibilityLabel:@"RepositoriesList"];
    [self.tableView setIsAccessibilityElement:YES];

    _language = defaultLanguage;
    _page = initialPage;
    moreItens = NO;
    
    [self fetchRepositories:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.repositories.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RepositoriesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];

#ifdef DEBUG
    [cell setAccessibilityLabel:[NSString stringWithFormat:@"Section %ld Row %ld", (long)indexPath.section, (long)indexPath.row]];
#endif
    
    cell.repositories = self.repositories[(NSUInteger)indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height){
        if(moreItens){
            [activity startAnimating];
            [self.tableView.tableFooterView setHidden:NO];
            [self loadMoreRepositories];
        }
    }
}

-(void)fetchRepositories:(UIRefreshControl *)refreshControl {
    self.page = initialPage;
    moreItens = NO;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    [RepositoriesService allRepositoriesfetchRepositoriesByLanguage:self.language forPage:self.page].then(^(NSArray *repositories) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

        if ([repositories count] == 0) {
            [refreshControl endRefreshing];
            return;
        }
        self.repositories = [[NSMutableArray alloc] initWithArray:repositories];
        [refreshControl endRefreshing];
        [self.tableView reloadData];
        moreItens = YES;
    }).catch(^(NSError *error) {
        NSLog(@"Error accessing repositories: %@", error);
    });
}

- (void)loadMoreRepositories {
    self.page++;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    [RepositoriesService allRepositoriesfetchRepositoriesByLanguage:self.language forPage:self.page].then(^(NSArray *repositories) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

        if ([repositories count] == 0) {
            [self.tableView.tableFooterView setHidden:YES];
            return;
        }
        [self.repositories addObjectsFromArray:repositories];
        [self.tableView.tableFooterView setHidden:YES];
        [self.tableView reloadData];
    }).catch(^(NSError *error) {
        NSLog(@"Error accessing repositories: %@", error);
    });
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *selectedPath = self.tableView.indexPathForSelectedRow;
    
    PullRequestViewController *pullrequest =  [segue destinationViewController];
    pullrequest.repository = self.repositories[(NSUInteger)selectedPath.row];
}


@end
