//
//  PullRequestViewController.h
//  zazcar-desafio
//
//  Created by Marcus Ataide on 31/03/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullRequestTableViewCell.h"
#import "PullRequestTableViewHeaderCell.h"
#import "PullRequestService.h"
#import "PullRequestModel.h"
#import "RepositoriesModel.h"
#import "WebViewController.h"

@interface PullRequestViewController : UITableViewController<UITableViewDataSource,UITableViewDelegate>
{
    int openedCount;
    int closedCount;
}

@property (readwrite, nonatomic, strong) NSMutableArray *pullrequests;
@property (nonatomic, strong) RepositoriesModel *repository;


@end
