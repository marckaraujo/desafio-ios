//
//  RepositoriesViewController.h
//  zazcar-desafio
//
//  Created by Marcus Ataide on 30/03/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RepositoriesTableViewCell.h"
#import "RepositoriesModel.h"
#import "RepositoriesService.h"
#import "PullRequestViewController.h"

@interface RepositoriesViewController : UITableViewController<UITableViewDataSource, UITableViewDelegate> {
    UIActivityIndicatorView *activity;
    BOOL moreItens;
}

@property (readwrite, nonatomic, strong) NSMutableArray *repositories;
@property (nonatomic, strong, readwrite) NSString *language;
@property(nonatomic, assign) int page;

@end
