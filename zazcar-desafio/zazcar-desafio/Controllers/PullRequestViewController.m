//
//  PullRequestViewController.m
//  zazcar-desafio
//
//  Created by Marcus Ataide on 31/03/16.
//  Copyright © 2016 Marcus Ataide. All rights reserved.
//

#import "PullRequestViewController.h"

static NSString * const reuseIdentifier = @"PullRequestTableViewCell";

@interface PullRequestViewController ()

@end

@implementation PullRequestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = NO;
    self.navigationItem.title = @"Nome Repositório";

    //CustomLeftBaritem
    UIButton *presetButton = [[UIButton alloc] initWithFrame:CGRectMake(180, 0, 30, 30)];
    UIImage *presetBtnBG = [[UIImage imageNamed:@"left"] stretchableImageWithLeftCapWidth:12.0 topCapHeight:0.0];
    [presetButton setImage:presetBtnBG forState:UIControlStateNormal];
    UIBarButtonItem *presetButtonItem = [[UIBarButtonItem alloc] initWithCustomView:presetButton];
    [presetButton addTarget:self action:@selector(toggleLeftPanel:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = presetButtonItem;
    
    [self allPullRequests];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.pullrequests.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PullRequestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];
    [cell setPullRequest:self.pullrequests[(NSUInteger)indexPath.row]];
    return cell;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    PullRequestTableViewHeaderCell *headerView = [tableView dequeueReusableCellWithIdentifier:@"PullRequestTableViewHeaderCell"];
    headerView.opened.text = [NSString stringWithFormat:@"%d opened", openedCount];
    headerView.closed.text = [NSString stringWithFormat:@"%d closed", closedCount];
    return headerView.contentView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(void)allPullRequests {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    [PullRequestService allPullRequestsWithOwner:self.repository.authorName forRepository:self.repository.name].then(^(NSArray *pullRequests) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

        if ([pullRequests count] == 0) {
            return;
        }
        for (PullRequestModel *model in pullRequests) {
            if ([model.state isEqualToString:@"open"]) {
                openedCount ++;
            } else {
                closedCount++;
            }
        }
        self.pullrequests = [[NSMutableArray alloc] initWithArray:pullRequests];
        [self.tableView reloadData];
    }).catch(^(NSError *error) {
        NSLog(@"Error accessing repositories: %@", error);
    });
}


#pragma mark - Navigation

- (void)toggleLeftPanel:(__unused id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
 
 
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *selectedPath = self.tableView.indexPathForSelectedRow;
    PullRequestModel *pullRequest = self.pullrequests[selectedPath.row];
    WebViewController *webView =  [segue destinationViewController];
    webView.stringURL = pullRequest.htmlUrl;
}


@end
